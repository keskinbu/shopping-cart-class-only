package com.trendyol.shoppingcart;

import com.trendyol.shoppingcart.utils.TestObject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CampaignTest {
    @Test
    void testAddCampaignsToCartAndCalculateDiscount()
    {
        ShoppingCart cart = TestObject.getCartWithItems();
        Category category = TestObject.getCategory();

        Campaign campaign1 = new Campaign(category, 20.0, 3, DiscountType.Rate);
        Campaign campaign2 = new Campaign(category, 50.0,5,DiscountType.Rate);
        Campaign campaign3 = new Campaign(category, 5.0,5,DiscountType.Amount);

        cart.applyDiscounts(campaign1, campaign2, campaign3);

        assertEquals(cart.getCampaignDiscount(), 80.0);
        assertEquals(cart.getTotalAmountAfterDiscounts(), 120.0);
    }
}