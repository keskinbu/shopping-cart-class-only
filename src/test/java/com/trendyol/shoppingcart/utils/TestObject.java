package com.trendyol.shoppingcart.utils;

import com.trendyol.shoppingcart.*;

public class TestObject {
    public static Category getCategory()
    {
        return new Category("food");
    }

    public static Category getCategoryWithParent()
    {
        Category categoryFood = new Category("food");
        return new Category("fruit", categoryFood);
    }

    public static Product getProduct(String name)
    {
        return new Product(name, 100.0, getCategory());
    }

    public static ShoppingCart getCartWithItems()
    {
        Product productApple = TestObject.getProduct("Apple");
        Product productAlmond = TestObject.getProduct("Almond");

        ShoppingCart cart = new ShoppingCart();

        cart.addItem(productApple, 3);
        cart.addItem(productAlmond, 1);

        return cart;
    }

    public static ShoppingCart getCartWithDiscounts()
    {
        ShoppingCart cart = getCartWithItems();
        Category category = TestObject.getCategory();

        Campaign campaign1 = new Campaign(category, 20.0, 3, DiscountType.Rate);
        Campaign campaign2 = new Campaign(category, 50.0,5,DiscountType.Rate);
        Campaign campaign3 = new Campaign(category, 5.0,5,DiscountType.Amount);

        Coupon coupon = new Coupon(100, 10, DiscountType.Rate);

        cart.applyDiscounts(campaign1, campaign2, campaign3);
        cart.applyCoupon(coupon);

        return cart;
    }
}
