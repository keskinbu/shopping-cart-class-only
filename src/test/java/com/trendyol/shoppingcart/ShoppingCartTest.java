package com.trendyol.shoppingcart;

import com.trendyol.shoppingcart.utils.TestObject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ShoppingCartTest {
    @Test
    void testAddItem() {
        Product productApple = TestObject.getProduct("Apple");
        Product productAlmond = TestObject.getProduct("Almond");

        ShoppingCart cart = new ShoppingCart();

        cart.addItem(productApple, 3);
        cart.addItem(productAlmond, 1);

        assertEquals(cart.getItems().size(), 2);
    }

    @Test
    void testAddCampaignsThenAddCoupon()
    {
        Category category = TestObject.getCategory();
        Product productApple = TestObject.getProduct("Apple");
        Product productAlmond = TestObject.getProduct("Almond");

        ShoppingCart cart = new ShoppingCart();

        cart.addItem(productApple, 3);
        cart.addItem(productAlmond, 1);

        Campaign campaign1 = new Campaign(category, 20.0, 3, DiscountType.Rate);
        Campaign campaign2 = new Campaign(category, 50.0,5,DiscountType.Rate);
        Campaign campaign3 = new Campaign(category, 5.0,5,DiscountType.Amount);

        Coupon coupon = new Coupon(100, 10, DiscountType.Rate);

        cart.applyDiscounts(campaign1, campaign2, campaign3);
        cart.applyCoupon(coupon);

        assertEquals(cart.getCampaignDiscount(), 80.0);
        assertEquals(cart.getCouponDiscount(), 40.0);
        assertEquals(cart.getTotalAmountAfterDiscounts(), 280.0);
    }

    @Test
    void testAddCouponThenAddCampaigns()
    {
        Category category = TestObject.getCategory();
        Product productApple = TestObject.getProduct("Apple");
        Product productAlmond = TestObject.getProduct("Almond");

        ShoppingCart cart = new ShoppingCart();

        cart.addItem(productApple, 3);
        cart.addItem(productAlmond, 1);

        Campaign campaign1 = new Campaign(category, 20.0, 3, DiscountType.Rate);
        Campaign campaign2 = new Campaign(category, 50.0,5,DiscountType.Rate);
        Campaign campaign3 = new Campaign(category, 5.0,5,DiscountType.Amount);

        Coupon coupon = new Coupon(100, 10, DiscountType.Rate);

        cart.applyCoupon(coupon);
        cart.applyDiscounts(campaign1, campaign2, campaign3);

        assertEquals(cart.getCampaignDiscount(), 80.0);
        assertEquals(cart.getCouponDiscount(), 40.0);
        assertEquals(cart.getTotalAmountAfterDiscounts(), 280.0);
    }

    @Test
    void testPrint()
    {
        ShoppingCart cart = TestObject.getCartWithDiscounts();

        cart.print();
    }
}