package com.trendyol.shoppingcart;

import com.trendyol.shoppingcart.utils.TestObject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CouponTest {
    @Test
    void testAddCoupon()
    {
        ShoppingCart cart = TestObject.getCartWithItems();

        Coupon coupon = new Coupon(100, 10, DiscountType.Rate);

        cart.applyCoupon(coupon);

        assertEquals(cart.getCouponDiscount(), 20.0);
        assertEquals(cart.getTotalAmountAfterDiscounts(), 180.0);
    }
}