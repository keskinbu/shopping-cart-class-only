package com.trendyol.shoppingcart;

import com.trendyol.shoppingcart.utils.TestObject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeliveryCostCalculatorTest {
    @Test
    void testCalculateForCart() {
        ShoppingCart cart = TestObject.getCartWithDiscounts();

        DeliveryCostCalculator deliveryCostCalculator = new DeliveryCostCalculator(5.00, 2.00);
        deliveryCostCalculator.calculateFor(cart);

        assertEquals(cart.getDeliveryCost(), 11.99);
    }
}