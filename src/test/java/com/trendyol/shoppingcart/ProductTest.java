package com.trendyol.shoppingcart;

import com.trendyol.shoppingcart.utils.TestObject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProductTest {
    @Test
    void testAddProduct()
    {
        Product product = TestObject.getProduct("Apple");

        assertEquals(product.getTitle(), "Apple");
        assertEquals(product.getPrice(), 100.0);
        assertEquals(product.getCategory().getTitle(), "food");
    }
}