package com.trendyol.shoppingcart;

import com.trendyol.shoppingcart.utils.TestObject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CategoryTest {
    @Test
    void testAddCategory()
    {
        Category category = TestObject.getCategory();

        assertEquals(category.getTitle(), "food");
        assertNull(category.getParent());
    }

    @Test
    void testAddCategoryWithParent()
    {
        Category categoryFruit = TestObject.getCategoryWithParent();

        assertEquals(categoryFruit.getTitle(), "fruit");
        assertEquals(categoryFruit.getParent().getTitle(), "food");
    }
}