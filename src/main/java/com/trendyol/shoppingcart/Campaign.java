package com.trendyol.shoppingcart;

public class Campaign {
    private Category category;

    private double discount;

    private String  discountType;

    private Integer minProductCount;

    public Campaign(Category category, double discount, Integer minProductCount, String discountType) {
        this.category = category;
        this.discount = discount;
        this.discountType = discountType;
        this.minProductCount = minProductCount;
    }

    public Category getCategory() {
        return category;
    }

    public double getDiscount() {
        return discount;
    }

    public String getDiscountType() {
        return discountType;
    }

    public Integer getMinProductCount() {
        return minProductCount;
    }
}
