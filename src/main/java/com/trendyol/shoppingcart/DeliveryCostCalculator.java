package com.trendyol.shoppingcart;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeliveryCostCalculator {
    private double costPerDelivery;

    private double costPerProduct;

    private double fixedCost = 2.99;

    public DeliveryCostCalculator(double costPerDelivery, double costPerProduct) {
        this.costPerDelivery = costPerDelivery;
        this.costPerProduct = costPerProduct;
    }

    void calculateFor(ShoppingCart cart) {
        Map<String, Integer> productMap = new HashMap<>();
        Map<String, Integer> categoryMap = new HashMap<>();

        for (Item item : cart.getItems()) {
            Product product = item.getProduct();

            String productTitle = product.getTitle();
            String categoryTitle = product.getCategory().getTitle();

            createDistinctMap(productMap, productTitle);
            createDistinctMap(categoryMap, categoryTitle);
        }

        double deliveryCost = calculateCost(categoryMap.size(), productMap.size());

        cart.setDeliveryCost(deliveryCost);
    }

    private void createDistinctMap(Map<String, Integer> map, String key) {
        if (map.containsKey(key)) {
            Integer count = map.get(key);
            map.put(key, ++count);
        } else {
            map.put(key, 1);
        }
    }

    public double calculateCost(int categoryCount, int productCount) {
        return (costPerDelivery * categoryCount) + (costPerProduct * productCount) + fixedCost;
    }
}
