package com.trendyol.shoppingcart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShoppingCart {
    private List<Item> items = new ArrayList<>();

    private double totalAmount;

    private double totalAmountAfterDiscounts;

    private Coupon coupon;

    private double couponDiscount;

    private double campaignDiscount;

    private double deliveryCost;

    public void addItem(Product product, int quantity) {
        Item item = new Item(product, quantity);

        items.add(item);

        totalAmount += item.getProduct().getPrice() * quantity;
    }

    public List<Item> getItems() {
        return items;
    }

    public void applyDiscounts(Campaign... campaigns) {
        if (couponDiscount > 0) {
            resetDiscounts();
        }

        double minCartAmount = 0.0;

        for (Item item : this.getItems()) {
            double minProductAmount = 0.0;

            for (Campaign campaign : campaigns) {
                if (campaign.getCategory().getTitle() != item.getProduct().getCategory().getTitle()) {
                    continue;
                }

                if (item.getQuantity() < campaign.getMinProductCount()) {
                    continue;
                }

                double total = calculateTotalAmountAfterDiscount(
                        campaign.getDiscountType(),
                        campaign.getDiscount(),
                        item.getProduct().getPrice()
                );

                if (minProductAmount <= total) {
                    minProductAmount = total;
                }

                minCartAmount += minProductAmount;
            }
        }

        campaignDiscount = minCartAmount;
        totalAmountAfterDiscounts = getTotalAmount() - campaignDiscount;

        if (this.coupon != null) {
            applyCoupon(this.coupon);
        }
    }

    public void applyCoupon(Coupon coupon) {
        this.coupon = coupon;

        double totalAmount = this.getTotalAmount();

        if (totalAmount > coupon.getMinAmount()) {
            double total = calculateTotalAmountAfterDiscount(
                    coupon.getDiscountType(),
                    coupon.getDiscount(),
                    totalAmount
            );

            if (total > coupon.getMinAmount()) {
                couponDiscount = totalAmount - total;
                totalAmountAfterDiscounts = setTotalAmountAfterDiscounts();
            }
        }
    }

    public void resetDiscounts() {
        getTotalAmount();
        couponDiscount = 0.0;
        campaignDiscount = 0.0;
    }

    public double getTotalAmount() {
        totalAmount = 0.0;

        for (Item item : this.items) {
            totalAmount += item.getProduct().getPrice() * item.getQuantity();
        }

        return totalAmount;
    }


    public double getTotalAmountAfterDiscounts() {
        return totalAmountAfterDiscounts;
    }

    public double setTotalAmountAfterDiscounts() {
        totalAmountAfterDiscounts = totalAmount - campaignDiscount - couponDiscount;

        return totalAmountAfterDiscounts;
    }

    public double getCouponDiscount() {
        return couponDiscount;
    }

    public double getCampaignDiscount() {
        return campaignDiscount;
    }

    public double getDeliveryCost() {
        return deliveryCost;
    }

    public void setDeliveryCost(double deliveryCost) {
        this.deliveryCost = deliveryCost;
    }

    private double calculateTotalAmountAfterDiscount(String discountType, double discount, double price) {
        double total = 0.0;

        switch (discountType) {
            case DiscountType.Rate:
                total = price - ((price * discount) / 100);
                break;
            case DiscountType.Amount:
                total = price - discount;
                break;
        }

        return total;
    }

    void print() {
        Map<String, Map<String, Product>> categoryMap = new HashMap<>();

        for (Item item : this.getItems()) {
            Product product = item.getProduct();
            String categoryTitle = product.getCategory().getTitle();

            String productTitle = product.getTitle();
            int quantity = item.getQuantity();

            Map<String, Product> productMap;
            if (categoryMap.containsKey(categoryTitle)) {
                productMap = categoryMap.get(categoryTitle);
                if (productMap.containsKey(productTitle)) {
                    productMap.put(productTitle, product);
                } else {
                    productMap.put(productTitle, product);
                }

            } else {
                productMap = new HashMap<>();
                productMap.put(productTitle, product);
            }
            categoryMap.put(categoryTitle, productMap);
        }
    }
}
