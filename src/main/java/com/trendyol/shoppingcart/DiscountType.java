package com.trendyol.shoppingcart;

public class DiscountType {
    public static final String Rate = "Rate";

    public static final String Amount = "Amount";
}
