package com.trendyol.shoppingcart;

public class Coupon {
    private double discount;

    private String  discountType;

    private double minAmount;

    public Coupon(double minAmount, double discount, String discountType) {
        this.discount = discount;
        this.discountType = discountType;
        this.minAmount = minAmount;
    }

    public double getDiscount() {
        return discount;
    }

    public String getDiscountType() {
        return discountType;
    }

    public double getMinAmount() {
        return minAmount;
    }
}
