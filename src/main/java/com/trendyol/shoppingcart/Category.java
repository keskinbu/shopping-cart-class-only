package com.trendyol.shoppingcart;

public class Category {
    private String title;

    private Category parent;

    public Category(String title) {
        this.title = title;
    }

    public Category(String title, Category parent) {
        this.title = title;
        this.parent = parent;
    }

    public String getTitle() {
        return title;
    }

    public Category getParent() {
        return parent;
    }
}
